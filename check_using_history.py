def check_using_history(self, timestamp, batch_number, amount, pm_account):
    # Compactible with python 3
    # Just copy and paste in the main python code class available at
    # https://perfectmoney.com/acct/samples/python/class.txt
    # Also, you must do the following imports
    # import requests
    # from datetime import datetime
    starttimestamp = int(timestamp) - 86400
    endtimestamp = int(timestamp) + 86400
    startmonth = datetime.utcfromtimestamp(starttimestamp).strftime("%m")
    startday = datetime.utcfromtimestamp(starttimestamp).strftime("%d")
    startyear = datetime.utcfromtimestamp(starttimestamp).strftime("%Y")
    endmonth = datetime.utcfromtimestamp(endtimestamp).strftime("%m")
    endday = datetime.utcfromtimestamp(endtimestamp).strftime("%d")
    endyear = datetime.utcfromtimestamp(endtimestamp).strftime("%Y")
    url = f"https://perfectmoney.com/acct/historycsv.asp?AccountID={self.__account}&PassPhrase={self.__passwd}&startmonth={startmonth}&startday={startday}&startyear={startyear}&endmonth={endmonth}&endday={endday}&endyear={endyear}&paymentsreceived=1&batchfilter={batch_number}"
    response = requests.get(url)
    if response.status_code == 200:
        records = response.text.splitlines()
        for record in records:
            record_dict = record.split(",")
            if len(record_dict) > 5:
                if (
                    record_dict[2] == batch_number
                    and record_dict[3] == "USD"
                    and float(record_dict[4]) >= float(amount)
                    and record_dict[6] == self.__account
                ):
                    return True
    return False
